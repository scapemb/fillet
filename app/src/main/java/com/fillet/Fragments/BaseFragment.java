package com.fillet.Fragments;

import android.support.v4.app.Fragment;

import com.fillet.DI.IHasComponent;

/**
 * Created by PICA on 05.09.17.
 */

public class BaseFragment extends Fragment {
    @SuppressWarnings("unchecked")
    protected <T> T getComponent(Class<T> componentType) {
        return componentType.cast(((IHasComponent<T>)getActivity()).getComponent());
    }
}
