package com.fillet.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.TransitionInflater;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.fillet.Adapters.DirectoryAdapter;
import com.fillet.Events.OpenPathEvent;
import com.fillet.Events.SavePathEvent;
import com.fillet.MVP.Presenters.BaseDirectoryPresenter;
import com.fillet.MVP.Presenters.DirectoryPresenter;
import com.fillet.MVP.Views.DirectoryView;
import com.fillet.Models.FileDTO;
import com.fillet.R;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.MvpLceViewStateFragment;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.RetainingLceViewState;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by PICA on 04.09.17.
 */

public class DirectoryFragment extends MvpLceViewStateFragment<SwipeRefreshLayout, FileDTO, DirectoryView, BaseDirectoryPresenter> implements DirectoryView, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView mRecyclerDirectory;
    private Toolbar toolbar;
    private ImageView toolbarIcon;

    private boolean isChooseMode = false;

    private DirectoryAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar(view);

        //init views
        mRecyclerDirectory = view.findViewById(R.id.recycler_directory_content);
        contentView.setOnRefreshListener(this);

        //setup adapter
        mAdapter = new DirectoryAdapter(getContext());
        mAdapter.setOnItemClickListener(mOnItemClickListener);

        int scrollPosition = 0;
        if(null != savedInstanceState){
            scrollPosition = savedInstanceState.getInt("scroll");
        }
        setRecyclerViewLayoutManager(getActivity().getResources().getConfiguration().orientation, scrollPosition);
        mRecyclerDirectory.setLayoutManager(mLayoutManager);
        mRecyclerDirectory.setAdapter(mAdapter);

        loadData(false);
    }


    private void initToolBar(View view) {
        toolbar =  view.findViewById(R.id.toolbar);
        toolbarIcon =  view.findViewById(R.id.toolbar_icon);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            setSharedElementReturnTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.change_image_transition));
            setExitTransition(TransitionInflater.from(getActivity()).inflateTransition(android.R.transition.fade));

            toolbarIcon.setTransitionName(getActivity().getResources().getString(R.string.transition_view));
        }
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_directory, container, false);
    }

    private DirectoryAdapter.OnItemClickListener mOnItemClickListener = new DirectoryAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(FileDTO file, View transitionIcon) {
            if(null == file){
                getActivity().onBackPressed();
                return;
            }

            if(file.isDirectory()) {

                setSharedElementReturnTransition(TransitionInflater.from(
                        getActivity()).inflateTransition(R.transition.change_image_transition));
                setExitTransition(TransitionInflater.from(
                        getActivity()).inflateTransition(android.R.transition.fade));


                EventBus.getDefault().post(new OpenPathEvent(file.getFile().getAbsolutePath(), transitionIcon));
            }else{
                if(isChooseMode){
                    Toast.makeText(getContext(), "You cannot open file", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                Uri fileURI = FileProvider.getUriForFile(
                        getContext(),
                        getContext().getApplicationContext().getPackageName() + ".provider",
                        file.getFile());
                intent.setDataAndType(fileURI, file.getMimeType());
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            }
        }

        @Override
        public void onItemLongClick() {
            if(isChooseMode){
                Toast.makeText(getContext(), "You cannot choose files", Toast.LENGTH_SHORT).show();
                return;
            }
            ((AppCompatActivity)getContext()).startSupportActionMode(actionModeCallback);
        }
    };

    public void setRecyclerViewLayoutManager(int orientation, int scrollPosition) {
        switch (orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                mLayoutManager = new GridLayoutManager(getActivity(), 4);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
            default:
                mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                break;
        }

        mRecyclerDirectory.setLayoutManager(mLayoutManager);
        mRecyclerDirectory.scrollToPosition(scrollPosition);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("scroll", ((LinearLayoutManager) mRecyclerDirectory.getLayoutManager())
                .findFirstCompletelyVisibleItemPosition());
        super.onSaveInstanceState(outState);
    }

    @Override
    public BaseDirectoryPresenter createPresenter() {
        String path = getArguments().getString("path");
        this.isChooseMode = getArguments().getBoolean("isChoose");
        return new DirectoryPresenter(getContext(), path);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Override
    public void setData(FileDTO data) {
        toolbar.setTitle(data.getFile().getName());
        mAdapter.setDirectory(data);
        mAdapter.setChooseMode(isChooseMode);
        mAdapter.notifyDataSetChanged();
    }

    @Override public void showContent() {
        super.showContent();
        contentView.setRefreshing(false);
    }

    @Override public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        contentView.setRefreshing(false);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.loadDirectory(pullToRefresh);
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }


    @Override
    public FileDTO getData() {
        return mAdapter == null ? null : mAdapter.getDirectory();
    }

    @NonNull
    @Override
    public LceViewState<FileDTO, DirectoryView> createViewState() {
        setRetainInstance(true);
        return new RetainingLceViewState<>();
    }

    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add("Delete");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
            new AlertDialog.Builder(getContext())
                    .setMessage("Delete selected files?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            mAdapter.deleteSelected();
                            mode.finish();
                            loadData(false);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).show();

            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mAdapter.clearSelected();
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_main_choose:{
                Toast.makeText(getContext(), "New default path", Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new SavePathEvent(mAdapter.getDirectory().getFile().getAbsolutePath()));
                break;
            }
            case android.R.id.home:{
                getActivity().onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
