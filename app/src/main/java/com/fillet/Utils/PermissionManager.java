package com.fillet.Utils;

import android.Manifest;

/**
 * Created by PICA on 04.09.17.
 */

public class PermissionManager {

    private static final String WRITE_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final String READ_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    private static final String[] STORAGE = new String[] {WRITE_STORAGE, READ_STORAGE};

    public void getStoragePermission(PermissionCallback callback) {
        //Permissioner.checkPermission(activity, CAMERA_AND_MICROPHONE, permissionContactCallback);
    }

    public interface PermissionCallback{
        void onGranted();
        void onRefused();
    }
}
