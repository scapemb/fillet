package com.fillet.Utils;

import android.content.Context;

import com.fillet.Models.FileDTO;

import java.io.File;

/**
 * Created by PICA on 04.09.17.
 */

public class FileUtil {
    private Context mContext;
    private FileWatcher mWatcher;
    private FileDTO mCurrentFile;

    public FileUtil(Context context) {
        mContext = context;
        mCurrentFile = new FileDTO();
    }

    public void openFile(final String path){
        new Thread(new Runnable() {
            @Override
            public void run() {
                File file = new File(path);
                if (file.isDirectory() && file.canRead()) {
                    mCurrentFile.init(file);
                    if(null != mWatcher){
                        mWatcher.onFileLoaded(mCurrentFile);
                    }
                }else {
                    if(null != mWatcher){
                        mWatcher.onError();
                    }
                }
            }
        }).run();
    }


    public FileDTO getCurrentFile() {
        return mCurrentFile;
    }

    //Watchers

    public void addWatcher(FileWatcher watcher){
        this.mWatcher = watcher;
    }
    public void removeWatcher(){
        this.mWatcher = null;
    }

    public interface FileWatcher{
        void onFileLoaded(FileDTO file);
        void onError();
    }
}
