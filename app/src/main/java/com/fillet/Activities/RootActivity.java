package com.fillet.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.transition.TransitionInflater;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.fillet.DI.ConfigModule;
import com.fillet.DI.FilletComponent;
import com.fillet.Events.OpenPathEvent;
import com.fillet.Fragments.DirectoryFragment;
import com.fillet.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

public class RootActivity extends BaseActivity {

    @Inject
    ConfigModule mConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        if (savedInstanceState == null) {
            requestStoragePermissions();
        }
    }

    private FilletComponent mComponent;
    @Override
    protected void setupComponent(FilletComponent appComponent) {
        this.mComponent = appComponent;
        appComponent.inject(this);
    }

    @Override
    public FilletComponent getComponent() {
        return this.mComponent;
    }

    private Fragment getFragment() {
        return new DirectoryFragment();
    }

    private void openDirectory(String path){
        openDirectory(path, null);
    }

    private void openDirectory(String path, View transitionView){
        Fragment f = getFragment();

        Bundle bundle = new Bundle();
        bundle.putString("path", path);
        f.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_right, R.anim.exit_left, R.anim.enter_left, R.anim.exit_right)
                .add(R.id.container_fragment, f)
                .addToBackStack(path);

        if(null != transitionView){
            f.setSharedElementEnterTransition(TransitionInflater.from(this).inflateTransition(R.transition.change_image_transition));

            transaction.addSharedElement(transitionView, getResources().getString(R.string.transition_view));
        }

        transaction.commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(OpenPathEvent event) {
        openDirectory(event.path, event.transitionView);
    }

    private final int PERMISSION_REQUEST_CODE = 101;
    public void requestStoragePermissions() {
        ActivityCompat.requestPermissions(this,
                new String[] {
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                openDirectory( mConfig.provideConfigDTO().getDefaultPath() );
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() > 1){
            getSupportFragmentManager().popBackStackImmediate();
        }
        else{
            finish();   //TODO: implement upgoing
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_main_setting:{
                Intent intent = new Intent(this, ChooseActivity.class);
                startActivity(intent);
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return true;
    }
}
