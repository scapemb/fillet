package com.fillet.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.fillet.DI.FilletComponent;
import com.fillet.DI.IHasComponent;
import com.fillet.FilletApplication;

/**
 * Created by PICA on 05.09.17.
 */

public abstract class BaseActivity extends AppCompatActivity  implements IHasComponent<FilletComponent> {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent(((FilletApplication) getApplication()).getComponent());
    }

    protected abstract void setupComponent(FilletComponent appComponent);
}
