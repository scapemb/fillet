package com.fillet.Activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.fillet.DI.ConfigModule;
import com.fillet.DI.FilletComponent;
import com.fillet.Events.OpenPathEvent;
import com.fillet.Events.SavePathEvent;
import com.fillet.Fragments.DirectoryFragment;
import com.fillet.Models.ConfigDTO;
import com.fillet.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

public class ChooseActivity extends BaseActivity {

    @Inject
    ConfigDTO mConfig;
    @Inject
    ConfigModule mConfigModule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        Toast.makeText(this, "Press choose menu item", Toast.LENGTH_SHORT).show();

        if (savedInstanceState == null) {
            requestStoragePermissions();
        }
    }

    private FilletComponent mComponent;
    @Override
    protected void setupComponent(FilletComponent appComponent) {
        this.mComponent = appComponent;
        appComponent.inject(this);
    }

    @Override
    public FilletComponent getComponent() {
        return this.mComponent;
    }


    private Fragment getFragment() {
        return new DirectoryFragment();
    }

    private void openDirectory(String path){
        Fragment f = getFragment();

        Bundle bundle = new Bundle();
        bundle.putString("path", path);
        bundle.putBoolean("isChoose", true);
        f.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_right, R.anim.exit_left, R.anim.enter_left, R.anim.exit_right)
                .add(R.id.container_fragment, f)
                .addToBackStack(path);

        transaction.commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(OpenPathEvent event) {
        openDirectory(event.path);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SavePathEvent event) {
        mConfigModule.saveExternalInstance(new ConfigDTO(event.path));
        finish();
    }

    private final int PERMISSION_REQUEST_CODE = 101;
    public void requestStoragePermissions() {
        ActivityCompat.requestPermissions(this,
                new String[] {
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openDirectory( Environment.getExternalStorageDirectory().getPath() );
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() > 1){
            getSupportFragmentManager().popBackStackImmediate();
        }
        else{
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.choose_menu, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        return false;
    }

}
