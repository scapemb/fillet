package com.fillet.MVP.Views;

import com.fillet.Models.FileDTO;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

/**
 * Created by PICA on 05.09.17.
 */

public interface DirectoryView extends MvpLceView<FileDTO> {
}
