package com.fillet.MVP.Presenters;

import com.fillet.MVP.Views.DirectoryView;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

/**
 * Created by PICA on 05.09.17.
 */

public interface BaseDirectoryPresenter extends MvpPresenter<DirectoryView> {
    void loadDirectory(final boolean pullToRefresh);
}
