package com.fillet.MVP.Presenters;

import android.content.Context;

import com.fillet.MVP.Views.DirectoryView;
import com.fillet.Models.FileDTO;
import com.fillet.Utils.FileUtil;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

/**
 * Created by PICA on 05.09.17.
 */

public class DirectoryPresenter extends MvpBasePresenter<DirectoryView> implements BaseDirectoryPresenter, FileUtil.FileWatcher {

    private Context mContext;
    private String mPath;
    private FileUtil mFileUtil;

    public DirectoryPresenter(Context context, String path) {
        this.mContext = context;
        this.mPath = path;
        mFileUtil = new FileUtil(context);
        mFileUtil.addWatcher(this);
    }

    @Override
    public void loadDirectory(boolean pullToRefresh) {
        getView().showLoading(pullToRefresh);
        mFileUtil.openFile(this.mPath);
    }

    @Override
    public void onFileLoaded(FileDTO file) {
        if(isViewAttached()){
            getView().setData(file);
            getView().showContent();
        }
    }

    @Override
    public void onError() {
        getView().showError(null, false);
    }
}
