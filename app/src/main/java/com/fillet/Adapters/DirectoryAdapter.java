package com.fillet.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fillet.Models.FileDTO;
import com.fillet.R;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by PICA on 04.09.17.
 */

public class DirectoryAdapter extends RecyclerView.Adapter<DirectoryAdapter.DirectoryViewHolder> {

    private Context mContext;
    private boolean isChooseMode = false;
    private FileDTO mDirectory;
    private OnItemClickListener mOnItemClickListener;

    private boolean isSelectMode = false;

    private Map<String, FileDTO> mSelectedItems = new HashMap<>();

    public DirectoryAdapter(Context context) {
        this.mContext = context;
    }
    public void setDirectory(FileDTO directory) {
        mDirectory = directory;
    }

    public FileDTO getDirectory() {
        return mDirectory;
    }

    @Override
    public DirectoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_directory, parent, false);
        return new DirectoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DirectoryViewHolder holder, int position) {
        final FileDTO file = getItem(position);

        if (null == file) {
            holder.name.setText("...");
            holder.root.setBackgroundColor(Color.TRANSPARENT);
        }else {
            holder.root.setBackgroundColor(isItemSelected(file) ? Color.LTGRAY : Color.TRANSPARENT);


            holder.name.setText(file.getFile().getName());

            FileDTO.MediaTypeInfo mediaTypeInfo = file.getMediaTypeInfo();
            holder.type.setText(mediaTypeInfo.getMediaType());
            holder.icon.setImageDrawable(mContext.getResources().getDrawable(mediaTypeInfo.getDrawableIconRes()));

            holder.root.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if(null != mOnItemClickListener){
                        mOnItemClickListener.onItemLongClick();
                        isSelectMode = true;
                    }
                    return false;
                }
            });
        }
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isSelectMode && file != null){
                    toggleSelection(file);
                }else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.icon.setTransitionName(mContext.getResources().getString(R.string.transition_view));
                    }
                    if (null != mOnItemClickListener) {
                        mOnItemClickListener.onItemClick(file, holder.icon);
                    }
                }
            }
        });

    }

    private void toggleSelection(FileDTO file){
        if(!isItemSelected(file)) {
            mSelectedItems.put(file.getId(), file);
        }else{
            mSelectedItems.remove(file.getId());
        }
        notifyDataSetChanged();
    }

    private boolean isItemSelected(FileDTO file){
        return mSelectedItems.containsKey(file.getId());
    }

    public void deleteSelected(){
        isSelectMode = false;
        Iterator it = mSelectedItems.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            ((FileDTO)pair.getValue()).delete();
        }
        notifyDataSetChanged();
    }

    public void clearSelected(){
        mSelectedItems.clear();
        isSelectMode = false;
        notifyDataSetChanged();
    }

    private FileDTO getItem(int position){
        return null == mDirectory || position == 0 ? null : mDirectory.getChild(position - 1);
    }

    @Override
    public int getItemCount() {
        return (null == mDirectory ? 0 : mDirectory.getChildCount()) + 1;   //top item for back
    }

    static class DirectoryViewHolder extends RecyclerView.ViewHolder {
        public View root;
        public TextView name;
        public TextView type;
        public ImageView icon;

        public DirectoryViewHolder(View convertView) {
            super(convertView);
            root =  convertView.findViewById(R.id.root_view);
            name =  convertView.findViewById(R.id.name);
            type =  convertView.findViewById(R.id.type);
            icon =  convertView.findViewById(R.id.icon);
        }
    }

    public interface OnItemClickListener{
        void onItemClick(FileDTO file, View transitionIcon);
        void onItemLongClick();
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        this.mOnItemClickListener = listener;
    }

    public void setChooseMode(boolean chooseMode) {
        isChooseMode = chooseMode;
    }
}
