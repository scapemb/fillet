package com.fillet.Events;

import android.view.View;

/**
 * Created by PICA on 04.09.17.
 */

public class OpenPathEvent {
    public final String path;
    public final View transitionView;

    public OpenPathEvent(String path, View transitionView) {
        this.path = path;
        this.transitionView = transitionView;
    }
    public OpenPathEvent(String path) {
        this.path = path;
        this.transitionView = null;
    }
}
