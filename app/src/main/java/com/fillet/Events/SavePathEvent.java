package com.fillet.Events;

/**
 * Created by PICA on 04.09.17.
 */

public class SavePathEvent {
    public final String path;

    public SavePathEvent(String path) {
        this.path = path;
    }
}
