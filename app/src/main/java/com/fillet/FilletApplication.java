package com.fillet;

import android.app.Application;

import com.fillet.DI.AppModule;
import com.fillet.DI.ConfigModule;
import com.fillet.DI.DaggerFilletComponent;
import com.fillet.DI.FilletComponent;

/**
 * Created by PICA on 04.09.17.
 */

public class FilletApplication extends Application  {
    private FilletComponent sComponent;

    public FilletComponent getComponent() {
        return sComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sComponent = buildComponent();
    }

    protected FilletComponent buildComponent(){
        return  DaggerFilletComponent
                .builder()
                .appModule(new AppModule(this))
                .configModule(new ConfigModule(this))
                .build();
    }

/*    protected ApplicationComponent build(){
        return Dagger
    }*/
}
