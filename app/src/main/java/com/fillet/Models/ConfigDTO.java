package com.fillet.Models;

import android.os.Environment;

/**
 * Created by PICA on 05.09.17.
 */

public class ConfigDTO {
    private String mDefaultPath;

    public ConfigDTO() {
        this.mDefaultPath = Environment.getExternalStorageDirectory().getPath();
    }

    public ConfigDTO(String defaultPath) {
        mDefaultPath = defaultPath;
    }

    public String getDefaultPath() {
        return mDefaultPath;
    }

    public void setDefaultPath(String defaultPath) {
        mDefaultPath = defaultPath;
    }
}
