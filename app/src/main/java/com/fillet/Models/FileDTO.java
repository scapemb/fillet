package com.fillet.Models;

import android.net.Uri;
import android.support.annotation.Nullable;
import android.webkit.MimeTypeMap;

import com.fillet.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PICA on 05.09.17.
 */

public class FileDTO {
    private File file;
    private String id;
    private List<FileDTO> childs;
    private boolean isDirectory = false;
    private String extension;
    private String mimeType;
    private MediaTypeInfo mMediaTypeInfo;

    public FileDTO() {
    }

    public FileDTO(File file) {
        this.file = file;
        this.id = file.getAbsolutePath();
        this.isDirectory = file.isDirectory();

        initMediaData(file);
    }

    public void init(File file) {
        this.file = file;
        if(file.isDirectory()){
            this.isDirectory = true;
            this.childs = getChildList(file.listFiles());
        }

        initMediaData(file);
    }

    private void initMediaData(File file){
        if(this.isDirectory){
            this.mimeType = "dir";
        } else {
            Uri selectedUri = Uri.fromFile(file);
            this.extension = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
            this.mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(this.extension);
        }
        this.mMediaTypeInfo = new MediaTypeFactory(this.mimeType).getMediaTypeInfo();
    }

    private List<FileDTO> getChildList(File... childs ){
        List<FileDTO> childList = new ArrayList<>();
        for(int i = 0; i < childs.length; i++){
            childList.add(new FileDTO(childs[i]));
        }

        return childList;
    }

    public File getFile() {
        return file;
    }

    public String getId() {
        return id;
    }

    public List<FileDTO> getChilds() {
        return childs;
    }

    public int getChildCount(){
        if(null != childs){
            return childs.size();
        }
        return 0;
    }

    public @Nullable FileDTO getChild(int position){
        if(null != childs && getChildCount() > 0){
            return childs.get(position);
        }
        return null;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void delete(){
        if( file.exists() ) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        delete(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
            file.delete();
        }
    }
    private void delete(File file){
        if( file.exists() ) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        delete(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
            file.delete();
        }
    }

    public MediaTypeInfo getMediaTypeInfo() {
        return mMediaTypeInfo;
    }

    private String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public class MediaTypeInfo extends AbstractType {
        int mDrawableIconRes;
        String mMediaType;

        public MediaTypeInfo(int drawableIconRes, String mediaType) {
            mDrawableIconRes = drawableIconRes;
            mMediaType = mediaType;
        }

        @Override
        public int getDrawableIconRes() {
            return mDrawableIconRes;
        }

        @Override
        public String getMediaType() {
            return mMediaType;
        }

        @Override
        public MediaTypeInfo getMediaTypeInfo() {
            return this;
        }
    }

    private abstract class AbstractType {
        abstract int getDrawableIconRes();
        abstract String getMediaType();
        abstract MediaTypeInfo getMediaTypeInfo();
    }

    public class MediaTypeFactory extends AbstractType {

        private String mimeType;

        public MediaTypeFactory(String mimeType) {
            this.mimeType = mimeType;
        }

        @Override
        int getDrawableIconRes() {
            if(null != mimeType) {
                if (mimeType.startsWith("dir")) {
                    return R.drawable.ic_folder;
                } else if (mimeType.startsWith("image/")) {
                    return R.drawable.ic_image;
                } else if (mimeType.startsWith("audio/")) {
                    return R.drawable.ic_audio;
                } else if (mimeType.startsWith("text/") || mimeType.startsWith("application/")) {
                    return R.drawable.ic_doc;
                } else if (mimeType.startsWith("video/")) {
                    return R.drawable.ic_video;
                }
            }
            return R.drawable.ic_file;
        }

        @Override
        String getMediaType() {
            if(null != mimeType) {
                if (mimeType.startsWith("dir")) {
                    return "Folder";
                } else if (mimeType.startsWith("image/")) {
                    return "Image file";
                } else if (mimeType.startsWith("audio/")) {
                    return "Audio file";
                } else if (mimeType.startsWith("text/") || mimeType.startsWith("application/")) {
                    return "Document file";
                } else if (mimeType.startsWith("video/")) {
                    return "Video file";
                }
            }
            return "File";
        }

        @Override
        MediaTypeInfo getMediaTypeInfo() {
            return new MediaTypeInfo(getDrawableIconRes(), getMediaType());
        }

    }

}
