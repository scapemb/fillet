package com.fillet.DI;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.fillet.FilletApplication;
import com.fillet.Models.ConfigDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by PICA on 05.09.17.
 */

@Module
public class ConfigModule {
    protected Context mContext;
    protected ConfigDTO mConfig;

    @Inject
    public ConfigModule(FilletApplication application){
        this.mContext = application.getApplicationContext();
        this.mConfig = restoreInstance();
    }

    @Provides
    public ConfigDTO provideConfigDTO(){
        if(null == mConfig){
            mConfig = restoreInstance();
        }
        if(null == mConfig){
            mConfig = new ConfigDTO();
        }
        return this.mConfig;
    }

    private  final String PREFS_DATA = "com.fillet.dataCache";
    private SharedPreferences settings;
    private  SharedPreferences.Editor editor;

    @Provides
    @Singleton
    public boolean saveInstance(){
        setPreferences(mContext);

        Gson gson = new GsonBuilder().
                registerTypeAdapter(Uri.class, new UriSerializer()).
                create();
        String dataCache = gson.toJson(mConfig);

        editor.putString(PREFS_DATA, dataCache);
        editor.commit();
        return true;
    }

    @Provides
    @Singleton
    public boolean saveExternalInstance(ConfigDTO config){
        try {
            setPreferences(mContext);

            Gson gson = new GsonBuilder().
                    registerTypeAdapter(Uri.class, new UriSerializer()).
                    create();
            String dataCache = gson.toJson(config);

            editor.putString(PREFS_DATA, dataCache);
            editor.commit();

            this.mConfig = config;
            return true;
        }catch (UnsupportedOperationException e){
            e.printStackTrace();
        }
        return false;
    }

    ConfigDTO restoreInstance(){
        setPreferences(mContext);

        Gson gson = new GsonBuilder().
                registerTypeAdapter(Uri.class, new UriDeserializer()).
                create();
        String savedInstance = settings.getString(PREFS_DATA, "");

        if(null != savedInstance && !savedInstance.isEmpty() ){
            return gson.fromJson(savedInstance, ConfigDTO.class);
        }
        return null;
    }

    public void clearDataCache(Context context){
        setPreferences(context);
        this.mConfig = null;
        try {
            saveExternalInstance(this.mConfig);
        }catch (Exception e){e.printStackTrace();}
        editor.clear().commit();
    }

    private void setPreferences(Context context){
        if(settings == null){
            settings = context.getSharedPreferences(PREFS_DATA,
                    Context.MODE_PRIVATE );
        }
        editor = settings.edit();
    }

    public class UriSerializer implements JsonSerializer<Uri> {
        public JsonElement serialize(Uri src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.toString());
        }
    }

    public class UriDeserializer implements JsonDeserializer<Uri> {
        @Override
        public Uri deserialize(final JsonElement src, final Type srcType,
                               final JsonDeserializationContext context) throws JsonParseException {
            return Uri.parse(src.getAsString());
        }
    }
}
