package com.fillet.DI;

/**
 * Created by PICA on 05.09.17.
 */

public interface IHasComponent <T> {
    T getComponent();
}