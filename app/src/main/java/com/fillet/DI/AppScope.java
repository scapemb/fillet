package com.fillet.DI;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by PICA on 05.09.17.
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@interface AppScope {
}