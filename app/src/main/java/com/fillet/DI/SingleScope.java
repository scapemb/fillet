package com.fillet.DI;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by PICA on 05.09.17.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface SingleScope {}