package com.fillet.DI;

import com.fillet.Activities.ChooseActivity;
import com.fillet.Activities.RootActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by PICA on 05.09.17.
 */

@Singleton
@Component(modules = {AppModule.class, ConfigModule.class})
public interface FilletComponent {
    void inject(RootActivity activity);
    void inject(ChooseActivity activity);
}
