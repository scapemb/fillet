package com.fillet.DI;

import android.content.Context;

import com.fillet.FilletApplication;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by PICA on 05.09.17.
 */

@Module
public class AppModule {

    private FilletApplication mApplication;

    @Inject
    public AppModule(FilletApplication application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    FilletApplication provideFilletApplication() {
        return this.mApplication;
    }

    @Provides
    @Singleton
    @AppScope
    public Context provideAppContext() {
        return this.mApplication.getApplicationContext();
    }


}
